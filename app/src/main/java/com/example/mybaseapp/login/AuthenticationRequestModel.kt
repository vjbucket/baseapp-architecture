package com.example.mybaseapp.login

/**
 * Created by Vijay Kumar on 24/08/20.
 */

data class LoginRequestModel (
    val email:String,
    val password:String
)

data class SignUpRequestModel (
    val email:String,
    val mobile:String,
    val name:String,
    val password:String
)