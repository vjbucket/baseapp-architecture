package com.example.mybaseapp.login

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.example.mybaseapp.BaseActivity
import com.example.mybaseapp.R
import kotlinx.android.synthetic.main.activity_authentication.*

/**
 * Created by Vijay Kumar on 23/08/20.
 */
class AuthenticationActivity : BaseActivity() {

    lateinit var viewModel: AuthenticationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)

        viewModel = ViewModelProvider(this).get(AuthenticationViewModel::class.java)
    }

    override fun showProgress() {
        pb_auth.visibility = View.VISIBLE
        main_layout.visibility = View.GONE
    }

    override fun hideProgress() {
        pb_auth.visibility = View.GONE
        main_layout.visibility = View.VISIBLE
    }





}