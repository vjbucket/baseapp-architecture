package com.example.mybaseapp.login

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.mybaseapp.BaseFragment
import com.example.mybaseapp.R
import com.example.mybaseapp.databinding.FragmentSplashBinding
import com.example.mybaseapp.helper.Constants
import com.example.mybaseapp.helper.GenericMethods
import com.example.mybaseapp.home.HomeActivity

/**
 * Created by Vijay Kumar on 24/08/20.
 */
class SplashFragment: BaseFragment() {
    lateinit var binding: FragmentSplashBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSplashBinding.inflate(inflater)

        Handler().postDelayed({
            if (GenericMethods.isLoggedIn()) {
                Intent(activity as AuthenticationActivity, HomeActivity::class.java).apply {
                    flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                    context?.startActivity(this)
                    activity?.finish()
                }
            } else {
                findNavController().navigate(R.id.loginFragment)
            }

        },2000)
        return binding.root
    }
}