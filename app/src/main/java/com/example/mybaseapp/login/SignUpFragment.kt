package com.example.mybaseapp.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.example.mybaseapp.BaseFragment
import com.example.mybaseapp.R
import com.example.mybaseapp.databinding.FragmentSignupBinding
import com.example.mybaseapp.helper.Constants
import com.example.mybaseapp.helper.GenericMethods
import com.example.mybaseapp.home.HomeActivity

/**
 * Created by Vijay Kumar on 23/08/20.
 */
class SignUpFragment:BaseFragment() {

    lateinit var binding: FragmentSignupBinding
    lateinit var authenticationActivity: AuthenticationActivity
    lateinit var viewModel: AuthenticationViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSignupBinding.inflate(inflater)
        authenticationActivity = activity as AuthenticationActivity
        viewModel = authenticationActivity.viewModel

        binding.tvLinkLogin.setOnClickListener {
            findNavController().navigate(R.id.loginFragment)
        }

        binding.btnSignup.setOnClickListener {
            val signUpRequestModel = SignUpRequestModel(
                binding.inputEmail.text.toString(),
                binding.inputMobile.text.toString(),
                binding.inputName.text.toString(),
                binding.inputPass.text.toString()
            )

            authenticationActivity.showProgress()
            viewModel.signup(signUpRequestModel).observe(viewLifecycleOwner) {
                it?.let {
                    authenticationActivity.hideProgress()
                    if (it != SignupLoginResponseModel()) {
                        GenericMethods.saveLoginPreferences(it)
                        navigateAfterLogin(it.name!!)
                    }
                }
            }
        }
        return binding.root
    }

    private fun navigateAfterLogin(name: String) {
        Intent(activity as AuthenticationActivity, HomeActivity::class.java).apply {
            putExtra(Constants.BundleString.USER_NAME, name)
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            context?.startActivity(this)
            activity?.finish()
        }
    }
}