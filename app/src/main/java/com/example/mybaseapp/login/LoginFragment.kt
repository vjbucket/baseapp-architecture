package com.example.mybaseapp.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.example.mybaseapp.BaseFragment
import com.example.mybaseapp.R
import com.example.mybaseapp.databinding.FragmentLoginBinding
import com.example.mybaseapp.helper.Constants
import com.example.mybaseapp.helper.GenericMethods
import com.example.mybaseapp.home.HomeActivity

/**
 * Created by Vijay Kumar on 23/08/20.
 */
class LoginFragment:BaseFragment() {

    lateinit var binding: FragmentLoginBinding
    lateinit var authenticationActivity: AuthenticationActivity
    lateinit var viewModel: AuthenticationViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginBinding.inflate(inflater)

        authenticationActivity = activity as AuthenticationActivity
        viewModel =  authenticationActivity.viewModel

        binding.tvLinkToSignup.setOnClickListener {
            findNavController().navigate(R.id.signUpFragment)
        }

        binding.btnLogin.setOnClickListener {
            val loginRequestModel = LoginRequestModel(binding.inputEmail.text.toString(), binding.inputPass.text.toString())

            authenticationActivity.showProgress()
            viewModel.login(loginRequestModel).observe(viewLifecycleOwner) {
                it?.let {
                    authenticationActivity.hideProgress()
                    if (it != SignupLoginResponseModel()) {
                        GenericMethods.saveLoginPreferences(it)
                        navigateAfterLogin(it.name!!)
                    }
                }
            }

        }
        return binding.root
    }


    private fun navigateAfterLogin(name: String) {
        Intent(activity as AuthenticationActivity, HomeActivity::class.java).apply {
            putExtra(Constants.BundleString.USER_NAME, name)
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            context?.startActivity(this)
            activity?.finish()
        }
    }
}