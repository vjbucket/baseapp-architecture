package com.example.mybaseapp.login

/**
 * Created by Vijay Kumar on 24/08/20.
 */

data class SignupLoginResponseModel(
    val accessToken: String = "",
    val id: Int? = null,
    val mobile: String? = null,
    val refreshToken: String = "",
    val name:String? = null
)