package com.example.mybaseapp.login

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.mybaseapp.network.Repository
import com.example.mybaseapp.network.ResultWrapper
import kotlinx.coroutines.launch

/**
 * Created by Vijay Kumar on 24/08/20.
 */
class AuthenticationViewModel(application: Application): AndroidViewModel(application) {

    fun login(loginRequestModel: LoginRequestModel): MutableLiveData<SignupLoginResponseModel?>{
        val responseMLD: MutableLiveData<SignupLoginResponseModel?> = MutableLiveData(null)

        viewModelScope.launch {
            Repository.login(loginRequestModel).let {
                if (it is ResultWrapper.Success) {
                    responseMLD.value = it.value?.result
                } else {
                    responseMLD.value = SignupLoginResponseModel()
                }
            }
        }

        return responseMLD
    }

    fun signup(signUpRequestModel: SignUpRequestModel): MutableLiveData<SignupLoginResponseModel?>{
        val responseMLD: MutableLiveData<SignupLoginResponseModel?> = MutableLiveData(null)

        viewModelScope.launch {
            Repository.signup(signUpRequestModel).let {
                if (it is ResultWrapper.Success) {
                    responseMLD.value = it.value?.result
                } else {
                    responseMLD.value = SignupLoginResponseModel()
                }
            }
        }

        return responseMLD
    }
}