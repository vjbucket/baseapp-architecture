package com.example.mybaseapp.network

/**
 * Created by Vijay Kumar on 23/08/20.
 */
sealed class ResultWrapper<out T> {
    data class Success <out T> (val value: T): ResultWrapper<T>()

    data class HttpError(val code: Int? = null, val error: com.example.mybaseapp.helper.Error? = null) :
        ResultWrapper<Nothing>()

    data class Error(val errorMessage:String?): ResultWrapper<Nothing>()
}