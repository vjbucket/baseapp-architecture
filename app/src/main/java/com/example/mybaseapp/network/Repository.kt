package com.example.mybaseapp.network

import com.example.mybaseapp.BuildConfig
import com.example.mybaseapp.helper.Constants
import com.example.mybaseapp.helper.CoroutineFactory
import com.example.mybaseapp.helper.GenericResponse
import com.example.mybaseapp.home.ProfileDetailResponseModel
import com.example.mybaseapp.login.LoginRequestModel
import com.example.mybaseapp.login.SignUpRequestModel
import com.example.mybaseapp.login.SignupLoginResponseModel
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Modifier
import java.util.concurrent.TimeUnit

/**
 * Created by Vijay Kumar on 23/08/20.
 */
object Repository {

    private val customInterceptor = object : Interceptor {
        override fun intercept(chain: Interceptor.Chain): okhttp3.Response {

            return chain.proceed(chain.request().newBuilder()
                /*.addHeader("user-agent", userAgent.toString())
                .addHeader("device-type", "ANDROID")
                .addHeader("device-env", BuildConfig.BUILD_TYPE)
                .addHeader("device-version", BuildConfig.VERSION_NAME)
                .addHeader("app-version-code",BuildConfig.VERSION_CODE.toString())
                .addHeader("device-id",GenericMethods.getDeviceId()?:"")
                .addHeader("accept-language", (if (language.isNullOrEmpty()) "en" else language)!!)*/.build())
        }
    }

    private val builder = OkHttpClient.Builder()
        .addInterceptor(customInterceptor)
        .connectTimeout(2, TimeUnit.MINUTES)
        .readTimeout(2, TimeUnit.MINUTES)
        //.authenticator(TokenAuthenticator())
        .writeTimeout(2, TimeUnit.MINUTES)

    private val gson = GsonBuilder().excludeFieldsWithModifiers(Modifier.TRANSIENT).create()

    private val api: Api = getApi()

    private fun getApi(): Api {
        return if (BuildConfig.DEBUG) {
            val client: OkHttpClient = builder.addInterceptor(com.itkacher.okhttpprofiler.OkHttpProfilerInterceptor()).build()
            Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build().create(Api::class.java)
        } else {
            val client: OkHttpClient = builder.build()
            Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build().create(Api::class.java)
        }
    }

    suspend fun login(loginRequestModel: LoginRequestModel): ResultWrapper<GenericResponse<SignupLoginResponseModel?>?>? {
        return CoroutineFactory.executeNetworkRequest {
            api.login(loginRequestModel).execute()
        }
    }

    suspend fun signup(signupLoginResponseModel: SignUpRequestModel): ResultWrapper<GenericResponse<SignupLoginResponseModel?>?>? {
        return CoroutineFactory.executeNetworkRequest {
            api.signup(signupLoginResponseModel).execute()
        }
    }

    suspend fun getProfileDetails(accessToken: String): ResultWrapper<GenericResponse<ProfileDetailResponseModel?>?>? {
        return CoroutineFactory.executeNetworkRequest {
            api.getProfile(accessToken).execute()
        }
    }
}