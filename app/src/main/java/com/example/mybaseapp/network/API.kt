package com.example.mybaseapp.network

import com.example.mybaseapp.helper.GenericResponse
import com.example.mybaseapp.home.ProfileDetailResponseModel
import com.example.mybaseapp.login.LoginRequestModel
import com.example.mybaseapp.login.SignUpRequestModel
import com.example.mybaseapp.login.SignupLoginResponseModel
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

/**
 * Created by Vijay Kumar on 23/08/20.
 */

interface Api {

    @POST("/user/login")
    fun login(@Body loginRequestModel: LoginRequestModel): Call<GenericResponse<SignupLoginResponseModel?>?>

    @POST("/user/signup")
    fun signup(@Body loginRequestModel: SignUpRequestModel): Call<GenericResponse<SignupLoginResponseModel?>?>

    @GET("/user/profile")
    fun getProfile(@Header("x-access-token") accessToken: String): Call<GenericResponse<ProfileDetailResponseModel?>?>
}