package com.example.mybaseapp.home

import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.example.mybaseapp.BaseActivity
import com.example.mybaseapp.R
import kotlinx.android.synthetic.main.activity_home.*

/**
 * Created by Vijay Kumar on 24/08/20.
 */
class HomeActivity: BaseActivity() {

    lateinit var viewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

        viewModel.getProfileDetails()
    }

    override fun showProgress() {
        pb_auth.visibility = View.VISIBLE
        main_layout.visibility = View.GONE
    }

    override fun hideProgress() {
        pb_auth.visibility = View.GONE
        main_layout.visibility = View.VISIBLE
    }

}