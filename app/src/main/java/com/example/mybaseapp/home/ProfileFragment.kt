package com.example.mybaseapp.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.lifecycle.observe
import com.example.mybaseapp.BaseFragment
import com.example.mybaseapp.R
import com.example.mybaseapp.databinding.FragmentProfileBinding
import com.example.mybaseapp.helper.Constants
import com.example.mybaseapp.login.AuthenticationActivity

/**
 * Created by Vijay Kumar on 24/08/20.
 */
class ProfileFragment:BaseFragment() {

    lateinit var binding: FragmentProfileBinding
    lateinit var homeActivity: HomeActivity
    lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfileBinding.inflate(inflater)
        homeActivity = activity as HomeActivity
        homeViewModel = homeActivity.viewModel

        binding.btnLogout.setOnClickListener {
            userLogout()
        }

        homeViewModel.profileResponseModel.observe(viewLifecycleOwner) {
            it?.let {
                if (it != ProfileDetailResponseModel()) {
                    binding.profile = it
                }
            }
        }
        return binding.root
    }

    private fun userLogout() {
        context?.getSharedPreferences(Constants.SharedPrefs.LOGIN_PREFERENCES, Context.MODE_PRIVATE)?.edit()?.clear()?.apply()

       Intent(homeActivity, AuthenticationActivity::class.java).apply {
           flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
           startActivity(this)
           homeActivity.finish()
       }

    }

}