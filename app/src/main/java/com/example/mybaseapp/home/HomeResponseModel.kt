package com.example.mybaseapp.home

/**
 * Created by Vijay Kumar on 24/08/20.
 */

data class ProfileDetailResponseModel(
    val accessToken: String? = "",
    val age: Int? = 0,
    val email: String? = "",
    val experianScore: Long? = 0L,
    val gender: String? = "",
    val id: Long? = 0L,
    val isEmailVerified: Boolean = false,
    val languageCode: String? = "",
    val mobile: String? = "",
    val name: String? = "",
    val profilePicture: String? = "",
    val stateId: String? = "",
    val state:String? = null,
    val genderEnum : String? = null
)