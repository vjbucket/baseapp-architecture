package com.example.mybaseapp.home

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.mybaseapp.helper.Constants
import com.example.mybaseapp.helper.GenericMethods
import com.example.mybaseapp.network.Repository
import com.example.mybaseapp.network.ResultWrapper
import kotlinx.coroutines.launch

/**
 * Created by Vijay Kumar on 24/08/20.
 */
class HomeViewModel(application: Application): AndroidViewModel(application) {

    var profileResponseModel: MutableLiveData<ProfileDetailResponseModel?> = MutableLiveData(null)

    fun getProfileDetails(): MutableLiveData<ProfileDetailResponseModel?> {
        val accessToken = GenericMethods.getDataFromPreferences(Constants.SharedPrefs.LOGIN_PREFERENCES, Constants.SharedPrefs.ACCESS_TOKEN)
        viewModelScope.launch {
            if (accessToken.isNullOrBlank()) {
                //forceLogout()
                return@launch
            }
            Repository.getProfileDetails(accessToken)?.let {
                if (it is ResultWrapper.Success)
                    profileResponseModel.value = it.value?.result
                else {
                    profileResponseModel.value = ProfileDetailResponseModel()
                }
            }
        }
        return profileResponseModel
    }
}