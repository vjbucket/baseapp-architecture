package com.example.mybaseapp

import androidx.appcompat.app.AppCompatActivity

/**
 * Created by Vijay Kumar on 23/08/20.
 */
abstract class BaseActivity: AppCompatActivity() {

    abstract fun showProgress()

    abstract fun hideProgress()
}