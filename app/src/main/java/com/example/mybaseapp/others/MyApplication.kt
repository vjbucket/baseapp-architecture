package com.example.mybaseapp.others

import android.app.Application
import com.example.mybaseapp.helper.Constants.context

/**
 * Created by Vijay Kumar on 24/08/20.
 */
class MyApplication: Application() {

    companion object {
        lateinit var instance: MyApplication
    }

    override fun onCreate() {
        super.onCreate()

        instance = this
        context = applicationContext
    }
}