package com.example.mybaseapp.helper

import com.example.mybaseapp.network.ResultWrapper
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException

/**
 * Created by Vijay Kumar on 24/08/20.
 */
object CoroutineFactory {
    suspend fun <T> executeNetworkRequest(action: suspend () -> Response<T>): ResultWrapper<T?>? {
        return withContext(Dispatchers.IO) {
            try {
                val type = object : TypeToken<GenericResponse<T>>() {}.type
                val req = action.invoke()
                if (req.isSuccessful)
                    ResultWrapper.Success(req.body())
                else {
                    var apiResponse = ""
                    req.errorBody()?.charStream()?.forEachLine { s -> apiResponse += s }
                    val response = Gson().fromJson<GenericResponse<T>>(apiResponse, type)
                    ResultWrapper.HttpError(req.code(), response.error)
                }
            } catch (throwable: Throwable) {
                when (throwable) {
                    is IOException -> ResultWrapper.Error(throwable.message)
                    is HttpException -> ResultWrapper.HttpError(throwable.code(), Error(throwable.code().toString(), "0", emptyList(), throwable.message(), 0))
                    else -> ResultWrapper.Error(throwable.message)
                }
            }
        }
    }
}