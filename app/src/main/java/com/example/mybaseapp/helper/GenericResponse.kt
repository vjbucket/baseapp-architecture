package com.example.mybaseapp.helper

data class GenericResponse <T>(
    val error: Error?,
    val result: T,
    val status: Int?
)

data class Error(
    val code: String?,
    val developerCode: Any?,
    val errors: List<String>?,
    val message: String?,
    val time: Long?
)