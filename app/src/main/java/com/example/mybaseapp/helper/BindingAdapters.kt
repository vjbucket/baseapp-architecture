package com.example.mybaseapp.helper

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

/**
 * Created by Vijay Kumar on 23/08/20.
 */

@BindingAdapter("imageURL")
fun setImageFromURL(imageView: ImageView, imageURL: String?) {
    imageURL?.let {
        Glide.with(imageView.context).load(imageURL).into(imageView)
    }
}

@BindingAdapter("setHtml")
fun setHtml(textView: TextView, html: String?) {
    html?.let {
        textView.setHtml(html)
    }
}

@BindingAdapter("isVisible")
fun setVisibility(view: View, isVisible: Boolean) {
    if (isVisible) {
        view.visibility = View.VISIBLE
    } else {
        view.visibility = View.GONE
    }
}