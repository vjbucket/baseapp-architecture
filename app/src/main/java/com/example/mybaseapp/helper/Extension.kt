package com.example.mybaseapp.helper

import android.os.Build
import android.text.Html
import android.view.View
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.StyleRes
import androidx.core.content.ContextCompat
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.Double

/**
 * Created by Vijay Kumar on 23/08/20.
 */

fun TextView.setHtml(html: String) {
    text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(html.trim(), Html.FROM_HTML_MODE_COMPACT).trim()
    } else {
        Html.fromHtml(html.trim()).trim()
    }
}

fun String.isNumeric(): Boolean {

    try {
        Double.parseDouble(this)
    } catch (e: NumberFormatException) {
        return false
    }

    return true
}

fun TextInputEditText.cursorToEnd() {
    setSelection(text.toString().length)
}

fun View.setBackgroundClr(@ColorRes id: Int) {
    setBackgroundColor(ContextCompat.getColor(context, id))
}

fun TextView.setTextAppearanceExt(@StyleRes resId: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        setTextAppearance(resId)
    } else{
        setTextAppearance(context, resId)
    }
}

inline fun <reified T> fromJson(json: String): T {
    return Gson().fromJson(json, object: TypeToken<T>(){}.type)
}

fun toJson(any: Any): String {
    return Gson().toJson(any)
}