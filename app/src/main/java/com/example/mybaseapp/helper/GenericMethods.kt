package com.example.mybaseapp.helper

import android.content.Context
import com.example.mybaseapp.helper.Constants.context
import com.example.mybaseapp.login.SignupLoginResponseModel

/**
 * Created by Vijay Kumar on 24/08/20.
 */
object GenericMethods {

    fun saveDataToPreferences(nameOfPref: String, map: Map<String, String>) {
        val editor = context?.getSharedPreferences(nameOfPref, Context.MODE_PRIVATE)?.edit()
        map.forEach {
            editor?.putString(it.key, it.value)
        }
        editor?.apply()
    }

    fun getDataFromPreferences(nameOfPref: String, nameOfString: String): String? {
        val sharedPreferences = context?.getSharedPreferences(nameOfPref, Context.MODE_PRIVATE)
        return sharedPreferences?.getString(nameOfString, null)
    }

    fun saveLoginPreferences(it: SignupLoginResponseModel) {
        GenericMethods.saveDataToPreferences(
            Constants.SharedPrefs.LOGIN_PREFERENCES,
            mapOf(Pair(Constants.SharedPrefs.ACCESS_TOKEN, it.accessToken),
                Pair(Constants.SharedPrefs.LOGIN_STATUS, "true"),
                Pair(Constants.SharedPrefs.REFRESH_TOKEN, it.refreshToken),
                Pair(Constants.SharedPrefs.USER_ID, it.id.toString())))
    }

    fun isLoggedIn(): Boolean {
        return getDataFromPreferences(Constants.SharedPrefs.LOGIN_PREFERENCES, Constants.SharedPrefs.LOGIN_STATUS) == "true"
    }
}