package com.example.mybaseapp.helper

import android.content.Context

/**
 * Created by Vijay Kumar on 23/08/20.
 */
object Constants {

    var context : Context? = null

    object SharedPrefs {

        //Login preferences
        const val LOGIN_PREFERENCES = "LoginPreference"
        const val ACCESS_TOKEN = "ACCESS_TOKEN"
        const val REFRESH_TOKEN = "REFRESH_TOKEN"
        const val LOGIN_STATUS = "LOGIN_STATUS"
        const val USER_ID = "USER_ID"

    }

    object BundleString{
        const val USER_NAME = "USER_NAME"

    }
}